from django.conf import settings
from django.views.generic import TemplateView

from ....models import Team
from ..gorest.gorestapi import GoRestApi

# Create your views here.


# def home(request):
#    return render(request, 'home.html', {})


class HomeView(TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        GAPI = GoRestApi(settings.GOREST["url"], settings.GOREST["token"])
        context = super().get_context_data(**kwargs)
        context["Teams"] = Team.objects.all().values()
        context["Users"] = GAPI.get_users()
        # context["Users"] = User.api.all().values()
        return context
