from dataclasses import dataclass, fields


@dataclass(init=False)
class GorestuserIn:
    name: str
    email: str
    gender: str
    status: str

    def __init__(self, **kwargs):
        names = set([f.name for f in fields(self)])
        for k, v in kwargs.items():
            if k in names:
                setattr(self, k, v)


@dataclass
class Gorestuser(GorestuserIn):
    id: int
