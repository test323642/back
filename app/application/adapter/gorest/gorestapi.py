import logging
from dataclasses import asdict
from typing import List

import requests

from .entity.gorestuser import Gorestuser as User
from .entity.gorestuser import GorestuserIn as UserIn

logger = logging.getLogger(__name__)


class ValidationError(Exception):
    def __init__(self, message=None):
        self.message = message
        super().__init__(self.message)


class ApiError(Exception):
    def __init__(self, message="Input error", status_code=400):
        self.message = message
        self.status_code = status_code
        super().__init__(self.message)


class GoRestApi:
    _token: str = ""
    _url: str = ""
    _session: requests.Session = None

    def __enter__(self):
        return self

    def __exit__(self, *_):
        self.close()

    def close(self):
        return self._session.close()

    def __init__(self, url: str, token: str) -> None:
        self._token = token
        self._url = url
        self._session = requests.Session()
        self._session.headers = {
            "Authorization": f"Bearer {self._token}",
            "Content-Type": "application/json",
        }

    def _apicall(self, path: str, method: str = "GET", args=None) -> dict:
        api_return = {}
        try:
            response = self._session.request(method, f"{self._url}{path}", json=args)
            response.raise_for_status()
            # print(response.text)
            if response.text:
                api_return = response.json()
            else:
                api_return = {}

        except requests.ConnectionError as e:
            raise ApiError(e.__str__(), status_code=response.status_code)

        except requests.RequestException as e:
            try:
                api_return = {}
                api_ret = response.json()
                for row in api_ret:
                    api_return[row.get("field", "")] = row.get("message", "")
            except:
                api_return = e.__str__()

            raise ApiError(api_return, status_code=response.status_code)
 
        return api_return

    def get_users(self, page: int = 1) -> List[User]:
        json = self._apicall(f"/users?page={page}", "GET")
        # print(json)
        rett = []
        for item in json:
            rett.append(User(**item))
        return rett

    def get_user(self, id: int) -> User:
        return self._apicall(f"/users/{id}", "GET")

    def create_user(self, user: UserIn) -> User:
        return self._apicall("/users", "POST", asdict(user))

    def delete_user(self, id: int) -> User:
        
        return self._apicall(f"/users/{id}", "DELETE")

    def edit_user(self, id: int, user: UserIn) -> User:
        return self._apicall(f"/users/{id}", "PATCH", asdict(user))


__all__ = [
    "GoRestApi",
]
