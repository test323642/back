from django.urls import path

from .views import UserViewEditDelete, UserViewViewCreate

urlpatterns = [
    path("users/<int:pk>", UserViewEditDelete.as_view()),
    path("users", UserViewViewCreate.as_view()),
]
