from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import serializers, status, views, viewsets
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from ..gorest.entity import gorestuser
from ..gorest.gorestapi import ApiError, GoRestApi
from ..persistence.entity.team import Team


class TeamSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Team
        fields = "__all__"


class TeamsViewSet(viewsets.ModelViewSet):
    queryset = Team.objects.all()
    serializer_class = TeamSerializer


gorestapi = GoRestApi(
    "https://gorest.co.in/public/v2",
    "951cce61a9a66ee490f0817bfe652f7886095feef66a250fc85f855804d88874",
)


class UserSerializerIn(serializers.Serializer):
    name = serializers.CharField(max_length=200)
    email = serializers.EmailField(max_length=200)
    gender = serializers.ChoiceField(choices=["male", "female"])
    status = serializers.ChoiceField(choices=["active", "inactive"])


class UserSerializer(UserSerializerIn):
    id = serializers.IntegerField()


class UserViewViewCreate(views.APIView):
    permission_classes = [AllowAny]
    page_param = openapi.Parameter(
        "page", openapi.IN_QUERY, description="page number", type=openapi.TYPE_INTEGER
    )
    user_response = openapi.Response("response description", UserSerializer(many=True))

    @swagger_auto_schema(manual_parameters=[page_param], responses={200: user_response})
    def get(self, request) -> [UserSerializer]:
        try:
            users = gorestapi.get_users(page=request.GET.get("page", 1))
            serializer = UserSerializer(users, many=True)
            return Response(serializer.data)
        except ApiError as e:
            return Response(data=e.message, status=e.status_code)

    @swagger_auto_schema(
        request_body=UserSerializerIn,
        responses={200: openapi.Response("", UserSerializer)},
    )
    def post(self, request):
        serializer = UserSerializerIn(data=request.data)
        if serializer.is_valid():
            userin = gorestuser.GorestuserIn(**serializer.data)
            try:
                user = gorestapi.create_user(userin)
            except ApiError as e:
                return Response(data=e.message, status=e.status_code)
        else:
            return Response(serializer.errors, status=422)
        serializer = UserSerializer(data=user)
        serializer.is_valid()
        return Response(serializer.data)


class UserViewEditDelete(views.APIView):
    permission_classes = [AllowAny]

    @swagger_auto_schema(
        request_body=UserSerializerIn,
        responses={200: openapi.Response("", UserSerializer)},
    )
    def put(self, request, pk=None) -> UserSerializer:
        serializer = UserSerializerIn(data=request.data)
        if serializer.is_valid():
            userin = gorestuser.GorestuserIn(**request.data)
            try:
                user = gorestapi.edit_user(pk, userin)
            except ApiError as e:
                return Response(data=e.message, status=e.status_code)
        else:
            return Response(serializer.errors, status=422)
        serializer = UserSerializer(data=user)
        serializer.is_valid()
        return Response(serializer.data)

    def delete(self, request, pk, format=None):
        try:
            api_return = gorestapi.delete_user(pk)
        except ApiError as e:
            return Response(data=e.message, status=e.status_code)
        return Response(None, status=status.HTTP_204_NO_CONTENT)
