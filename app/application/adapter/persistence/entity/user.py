from dataclasses import asdict

from django.conf import settings
from django.db import models

from ...gorest.gorestapi import GoRestApi

gapi = GoRestApi(settings.GOREST["url"], settings.GOREST["token"])


class UserManager(models.Manager):
    def get_queryset(self):
        none_qs = User.objects.none()

        for item in gapi.get_users():
            # user =
            none_qs.create(**asdict(item))
            # user = User(**dict(item))
            # users.append(User(**asdict(item)))
        #    qs |= User.objects.filter(id=item.id)
        # qs = list(chain(none_qs, users))

        return none_qs
        # return gapi.get_users()


class User(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255)
    email = models.EmailField(max_length=255)
    gender = models.CharField(max_length=255)
    status = models.CharField(max_length=255)

    api = UserManager()
    objects = models.Manager()

    def __str__(self) -> str:
        return self.name

    class Meta:
        managed = False
        db_table = "notable"
