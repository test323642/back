from uuid import uuid4

from django.db import models


class Team(models.Model):
    team_id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    name = models.CharField(max_length=200)

    def __str__(self) -> str:
        return self.name

    class Meta:
        db_table = "team_team"
